#!/bin/bash

set -Eeuo pipefail
shopt -s inherit_errexit

# expects the following variables defined in the environment:
# - GRAFANA_{URL,TOKEN}: Grafana credentials
# - GITLAB_PROJECT_URL: git repo without protocol
# - GITLAB_FORK_URL: fork of the git repo without protocol
# - GITLAB_TOKEN: GitLab private token with access to the git repo
# - BRANCH_NAME: branch name in the fork of the git repo
# - SOURCE_BRANCH_NAME: source branch name to checkout, by default `main`
# - GRAFANA_DATA_DIR: directory in the git repo to hold the Grafana data
# - GIT_USER_NAME: Git user name
# - GIT_USER_EMAIL: Git user email

# shellcheck disable=SC1091
. cki_utils.sh

BASE_DIR=$(mktemp -d)
trap 'rm -rf "${BASE_DIR}"' EXIT

DATA_PATH="${BASE_DIR}/repo"
mkdir -p "${DATA_PATH}"
cd "${DATA_PATH}"

cki_echo_yellow "Fetching repository..."
if ! [[ -d .git ]]; then
    # shellcheck disable=SC2154
    git clone "https://ignored:${GITLAB_TOKEN}@${GITLAB_PROJECT_URL}.git/" .
    cki_echo_green "  successfully cloned main repository"
    # shellcheck disable=SC2154
    git remote add fork "https://ignored:${GITLAB_TOKEN}@${GITLAB_FORK_URL}.git/"
    # shellcheck disable=SC2154
    git config user.name "${GIT_USER_NAME}"
    # shellcheck disable=SC2154
    git config user.email "${GIT_USER_EMAIL}"
fi
git fetch --all
cki_echo_green "  successfully fetched updates"

# shellcheck disable=SC2154
cki_echo_yellow "Checking out '${BRANCH_NAME}'"
if git show-ref --verify --quiet "refs/remotes/fork/${BRANCH_NAME}"; then
    git switch --track "fork/${BRANCH_NAME}"
    git reset --hard "fork/${BRANCH_NAME}"
    cki_echo_green "  switched to branch"
    git rebase origin/HEAD
    cki_echo_green "  rebased to origin/HEAD"
else
    git checkout --track "origin/${SOURCE_BRANCH_NAME:-main}" -b "${BRANCH_NAME}"
    git config "branch.${BRANCH_NAME}.merge" "refs/heads/${BRANCH_NAME}"
    git config "branch.${BRANCH_NAME}.remote" fork
    cki_echo_green "  created branch"
fi

cki_echo_yellow "Downloading from Grafana"
# shellcheck disable=SC2154
python3 -m cki.deployment_tools.grafana download --path "${GRAFANA_DATA_DIR}"
    cki_echo_green "  completed"

cki_echo_yellow "Checking for changes"
git add "${GRAFANA_DATA_DIR}"
if ! git diff --stat --cached --exit-code; then
    timestamp=$(date +'%F %H:%M')
    git commit -m "Grafana updates as of ${timestamp}"
    git push \
        --force \
        --push-option merge_request.create \
        --push-option merge_request.title="Grafana updates" \
        --push-option merge_request.remove_source_branch \
        fork
    cki_echo_green "  committed and pushed changes"
elif ! git log origin/"${BRANCH_NAME}" ^HEAD --quiet --exit-code; then
    cki_echo_green "  updating rebased branch"
    git push --force
else
    cki_echo_green "  no changes found"
fi
