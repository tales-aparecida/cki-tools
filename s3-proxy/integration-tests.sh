#!/bin/bash

set -Eeuo pipefail
shopt -s inherit_errexit

# minio
export MINIO_ROOT_USER=user
export MINIO_ROOT_PASSWORD=password

# proxy
export AWS_ACCESS_KEY_ID=user
export AWS_SECRET_ACCESS_KEY=password
export AWS_ENDPOINT_HOST=minio:9000
export AWS_ENDPOINT_SCHEME=http
export AWS_DEFAULT_REGION=us-east-1

temp_dir=$(mktemp -d)
export temp_dir
trap 'rm -rf "${temp_dir}"' EXIT

aws_minio=(aws --endpoint-url "http://${MINIO_HOST:-localhost}:9000")
aws_proxy=(aws --endpoint-url "http://${PROXY_HOST:-localhost}:8000" --no-sign-request)

# create integration test bed
echo Hello World > "${temp_dir}/expected-file.txt"
"${aws_minio[@]}" s3 mb s3://integration-test || true
"${aws_minio[@]}" s3 cp "${temp_dir}/expected-file.txt" "s3://integration-test/path/to/file with spaces.txt"
"${aws_minio[@]}" s3 cp "${temp_dir}/expected-file.txt" "s3://integration-test/another path/to/file with spaces.txt"

# set expectations
echo "integration-test" > "${temp_dir}/expected-list-buckets.txt"
echo "another path/to/file with spaces.txt" > "${temp_dir}/expected-list-objects.txt"
echo "path/to/file with spaces.txt" >> "${temp_dir}/expected-list-objects.txt"
echo "path/to/file with spaces.txt" > "${temp_dir}/expected-list-objects-prefix.txt"
echo "another path/" > "${temp_dir}/expected-list-objects-delimiter.txt"
echo "path/" >> "${temp_dir}/expected-list-objects-delimiter.txt"

# observe proxy
unset AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_ENDPOINT_HOST \
  AWS_ENDPOINT_SCHEME AWS_DEFAULT_REGION
"${aws_proxy[@]}" s3api list-buckets | jq -r '.Buckets[].Name' > "${temp_dir}/observed-list-buckets.txt"
"${aws_proxy[@]}" s3api list-objects --bucket integration-test | jq -r '.Contents[].Key' > "${temp_dir}/observed-list-objects.txt"
"${aws_proxy[@]}" s3api list-objects --bucket integration-test --prefix path/ | jq -r '.Contents[].Key' > "${temp_dir}/observed-list-objects-prefix.txt"
"${aws_proxy[@]}" s3api list-objects --bucket integration-test --delimiter / | jq -r '.CommonPrefixes[].Prefix' > "${temp_dir}/observed-list-objects-delimiter.txt"
"${aws_proxy[@]}" s3 cp "s3://integration-test/path/to/file with spaces.txt" "${temp_dir}/observed-file.txt"

# check results
diff -u "${temp_dir}/"{observed,expected}-list-buckets.txt
diff -u "${temp_dir}/"{observed,expected}-list-objects.txt
diff -u "${temp_dir}/"{observed,expected}-list-objects-prefix.txt
diff -u "${temp_dir}/"{observed,expected}-list-objects-delimiter.txt
