"""Datawarehouse UMB Submitter tests."""
import json
import os
import unittest
from unittest import mock

import pika

from cki.kcidb.datawarehouse_umb_submitter import Receiver
from cki.kcidb.datawarehouse_umb_submitter import main


class TestDatawarehouseUMBSubmitter(unittest.TestCase):
    """Test cki.kcidb.datawarehouse_umb_submitter."""

    body = {
        "version": {
            "major": 4,
            "minor": 0
        },
        "checkouts": [],
        "builds": [],
        "tests": [],
    }

    callback_kwargs = {
        'body': body,
        'routing_key': 'routing_key',
        'headers': None,
        'ack_fn': None,
        'timeout': False,
    }

    datawarehouse_environ = {
        'DATAWAREHOUSE_URL': 'datawarehouse_url',
        'DATAWAREHOUSE_TOKEN_SUBMITTER': 'datawarehouse_token',
    }

    @mock.patch.object(Receiver, 'callback')
    @mock.patch('pika.BlockingConnection')
    @mock.patch.dict(
        os.environ,
        {
            'RABBITMQ_HOST': 'host',
            'RABBITMQ_PORT': '123',
            'RABBITMQ_USER': 'user',
            'RABBITMQ_PASSWORD': 'password',
            'RABBITMQ_EXCHANGE': 'exchange',
            'RABBITMQ_QUEUE': 'queue',
            'RABBITMQ_ROUTING_KEYS': 'routing1 routing2',
            **datawarehouse_environ,
        }
    )
    def test_main(self, connection, callback):
        connection().channel().consume.return_value = [(
            mock.Mock(routing_key='routing_key', delivery_tag='delivery_tag'),
            pika.BasicProperties(),
            json.dumps(self.body),
        )]

        main()
        callback.assert_called_with(**self.callback_kwargs)

    @mock.patch.dict(os.environ, datawarehouse_environ)
    def test_callback_valid(self):
        """Test callback when passed valid KCIDB data."""
        receiver = Receiver()
        with mock.patch.object(receiver.dw_api.kcidb.submit, 'create') as mock_create:
            receiver.callback(**self.callback_kwargs)
            mock_create.assert_called_with(data=self.body)

    @mock.patch.dict(os.environ, datawarehouse_environ)
    def test_callback_invalid(self):
        """Test callback when passed invalid KCIDB data."""
        receiver = Receiver()
        with mock.patch.object(receiver.dw_api.kcidb.submit, 'create') as mock_create:
            receiver.callback(body={})
            self.assertFalse(mock_create.called)
