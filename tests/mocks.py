"""Mock helpers."""
import unittest

from cki.cki_tools import _utils


def get_mocked_bucket():
    """Get mocked S3Bucket."""
    mocked_bucket_spec = _utils.BucketSpec('endpoint', 'access_key',
                                           'secret_key', 'bucket', 'prefix')
    bucket = _utils.S3Bucket(mocked_bucket_spec)
    bucket.client = unittest.mock.Mock()
    return bucket
