# `cki.cki_tools.repo_manager`

Checkout all active CKI projects to a local directory.

With `--fork`, a personal fork and associated remote will be created if necessary.

With `--venv`, a Python venv will be setup via direnv and pip.

```shell
python3 -m cki.cki_tools.repo_manager
    [--directory DIRECTORY]
    [--force]
    [--fork REMOTE]
    [--venv]
```

## Environment variables

| Environment variable | Type   | Required | Description                                                           |
|----------------------|--------|----------|-----------------------------------------------------------------------|
| `GITLAB_TOKENS`      | dict   | yes      | URL/environment variable pairs of GitLab instances and private tokens |
| `GITLAB_TOKEN`       | string | yes      | GitLab private tokens as configured in `GITLAB_TOKENS`                |
