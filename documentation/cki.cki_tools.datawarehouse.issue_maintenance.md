# Datawarehouse - Issue Maintenance

`cki.cki_tools.datawarehouse.issue_maintenance`

Get a list of DataWarehouse issues requiring attention.

This helper script will go through the list of issues in
DataWarehouse and print information about the ones that
match any of the following conditions:

- Last tagged checkout run more than `$since` time ago.
- The ticket_url is linked to one of the known services (GitLab or Bugzilla)
  and it's state is `closed`.

```bash
$ python3 -m cki.cki_tools.datawarehouse.issue_maintenance -h
usage: issue_maintenance.py [-h] [OPTIONS]

options:
  -h, --help     show this help message and exit
  --since SINCE  Show issues not linked to a checkout in this period of time (default: 4 weeks)
  --only-closed  Only show issues with a closed ticket (default: False)
```

## Environment variables

| Name                  | Required | Description                                                            |
|-----------------------|----------|------------------------------------------------------------------------|
| `GITLAB_TOKENS`       | yes      | URL/environment variable pairs of GitLab instances and private tokens  |
| `GITLAB_TOKEN`        | yes      | GitLab private tokens as configured in `GITLAB_TOKENS` above           |
| `BUGZILLA_URL`        | no       | URL to the Bugzilla instance. (Default: https://bugzilla.redhat.com)   |
| `BUGZILLA_API_KEY`    | yes      | API key for Bugzilla.                                                  |
| `DATAWAREHOUSE_URL`   | no       | URL to DataWarehouse. (Default: https://datawarehouse.cki-project.org) |
| `DATAWAREHOUSE_TOKEN` | yes      | Token for DataWarehouse.                                               |
