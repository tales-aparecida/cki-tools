# `cki.cki_tools.orphan_hunter`

Check whether the GitLab jobs for Pods spawned by `gitlab-runner` are still
running. Pods of already finished jobs are deleted.

## Environment variables

| Name                | Secret | Required | Description                                                           |
|---------------------|--------|----------|-----------------------------------------------------------------------|
| `GITLAB_TOKENS`     | no     | yes      | URL/environment variable pairs of GitLab instances and private tokens |
| `GITLAB_TOKEN`      | yes    | yes      | GitLab private tokens as configured in `gitlab_tokens` above          |
| `IRCBOT_URL`        | no     | no       | IRC bot endpoint                                                      |
| `URL_SHORTENER_URL` | no     | no       | URL shortener endpoint to link to GitLab jobs in IRC messages         |
| `ACTION`            | no     | no       | `report` (default) or `delete` Pods                                   |

The orphan hunter service account need permissions equivalent to the following Role:

```yaml
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: orphan-hunter
rules:
  - apiGroups: [""]
    resources: [pods]
    verbs: [get, list, delete]
```
