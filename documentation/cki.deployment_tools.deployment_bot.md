# `cki.deployment_tools.deployment_bot`

Triggers deployment pipelines when messages for specified GitLab jobs come
along.

## Configuration

This bot reads the configuration from the `DEPLOYMENT_BOT_CONFIG` environment
variable. If `DEPLOYMENT_BOT_CONFIG` is not present, it will try to load the
config from the file specified in `--config-path` or
`DEPLOYMENT_BOT_CONFIG_PATH`.

Example configuration:

```yaml
irc-bot:
  .trigger:
    project_url: https://gitlab.com/cki-project/(?P<project>irc-bot)
    captures:
      foo: baz
  .deployment:
    project_url: https://deployment-instance/project
    ref: main
    external_url: dotenv:deploy.env:environment_url
  mr-start:
    trigger:
      object_kind: build
      build_status: success
      build_id: (?P<job_id>.*)
      environment:
        action: start
        name: (?P<mr>mr-[0-9]+)
    deployment:
      variables:
        ONLY_PROJECT_NAME: '{project}'
        CKI_STAGING_TAG: '{mr}'
        FOO: '{foo}'
```

## Configuring message filters

| Field                  | Description                                                                          |
|------------------------|--------------------------------------------------------------------------------------|
| `.trigger.project_url` | GitLab project URL that contains the GitLab environments, matched via `re.fullmatch` |
| `.trigger.captures`    | Additional captures that can be used in constructing deplomyent pipeline variables   |
| `<name>.trigger`       | Message filter that is matched to received messages via `re.fullmatch`               |

## Configuring deployment pipelines

All matching is done via `re.fullmatch`. Subexpressions captured via
`(?P<name>reg-ex)` and specified in `.trigger.captures` can be used to
construct the pipeline variables via Python formatting expressions like
`{name}`.

| Field                               | Description                                                                |
|-------------------------------------|----------------------------------------------------------------------------|
| `.deployment.project_url`           | GitLab project URL that contains the deployment pipelines                  |
| `.deployment.ref`                   | Branch or ref for the triggered pipelines, defaults to main                |
| `.deployment.external_url`          | dotenv:artifact-file:variable-name to read the external URL for the env    |
| `<name>.deployment.external_url`    | dotenv:artifact-file:variable-name to read the external URL for the env    |
| `<name>.deployment.message.success` | Message template to use when notifying about successful deployments        |
| `<name>.deployment.message.failed`  | Message template to use when notifying about failed deployments            |
| `<name>.deployment.variables`       | Pipeline variables, captures can be used via Python formatting expressions |

## Environment variables

| Environment variable         | Description                                                                        |
|------------------------------|------------------------------------------------------------------------------------|
| `CKI_DEPLOYMENT_ENVIRONMENT` | Define the deployment environment (production/staging)                             |
| `DEPLOYMENT_BOT_CONFIG`      | Configuration in YAML. If not present, falls back to `DEPLOYMENT_BOT_CONFIG_PATH`. |
| `DEPLOYMENT_BOT_CONFIG_PATH` | Path to the configuration YAML file (Default: config.yml)                          |
| `RABBITMQ_HOST`              | RabbitMQ host                                                                      |
| `RABBITMQ_PORT`              | RabbitMQ port, TLS is used for port 443                                            |
| `RABBITMQ_USER`              | RabbitMQ user                                                                      |
| `RABBITMQ_PASSWORD`          | RabbitMQ password                                                                  |
| `RABBITMQ_CAFILE`            | RabbitMQ CA file path                                                              |
| `RABBITMQ_CERTFILE`          | RabbitMQ certificate + private key file path                                       |
| `SENTRY_SDN`                 | Sentry SDN                                                                         |

### `CKI_DEPLOYMENT_ENVIRONMENT`

On staging developments (`CKI_DEPLOYMENT_ENVIRONMENT != production`), no
pipelines are triggered, and reporting only happens on the console and not on
IRC.
