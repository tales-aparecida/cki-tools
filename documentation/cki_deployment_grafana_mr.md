# `shell-scripts/cki_deployment_grafana_mr.sh`

Create MRs of changed data in Grafana instances via `cki.deployment_tools.grafana`.

```shell
cki_deployment_grafana_mr.sh
```

## Environment variables

| Field                | Type   | Required | Description                                        |
|----------------------|--------|----------|----------------------------------------------------|
| `GRAFANA_URL`        | string | yes      | Grafana instance URL                               |
| `GRAFANA_TOKEN`      | string | yes      | Grafana secret token                               |
| `GITLAB_PROJECT_URL` | string | yes      | Git repo URL without protocol                      |
| `GITLAB_FORK_URL`    | string | yes      | URL of the fork of the Git repo without protocol   |
| `GITLAB_TOKEN`       | string | yes      | GitLab private token with access to the Git repo   |
| `BRANCH_NAME`        | string | yes      | Branch name in the fork of the git repo            |
| `GRAFANA_DATA_DIR`   | string | yes      | Directory in the git repo to hold the Grafana data |
| `GIT_USER_NAME`      | string | yes      | Git user name                                      |
| `GIT_USER_EMAIL`     | string | yes      | Git user email                                     |

Changes need to be backwards-compatible as this script is also used by the OSCI
and TFT teams at
<https://gitlab.cee.redhat.com/osci/monitoring/-/blob/master/.gitlab-ci.yml#L265>!


