# `cki.deployment_tools.secrets` - accessing CKI secrets

The `secrets` tool allows access to CKI variables and secrets.

```text
$ python3 -m cki.deployment_tools.secrets --help
usage: secrets.py [-h] [--json] {encrypt,secret,variable} value

Access CKI variables and secrets

positional arguments:
  {encrypt,secret,variable}
  value

optional arguments:
  -h, --help            show this help message and exit
  --json                output in json format
```

The following CLI aliases are provided:
- reading secrets: `cki_secret` == `python3 -m cki.deployment_tools.secrets secret`
- reading variables: `cki_variable` == `python3 -m cki.deployment_tools.secrets variable`

Currently, CKI variables and secrets are stored in YAML files, falling back to
HashiCorp Vault for secrets if the `VAULT_ADDR` environment variable is defined.

## Secrets

For secrets from YAML files, the file name needs to be specified in the
`CKI_SECRETS_FILE` environment variable. A list of space-delimited additional
environment variable names can be provided in the `CKI_SECRETS_NAMES`
environment variable. In the secrets file, the values are encrypted via
OpenSSL. The encryption key needs to be provided in the `ENVPASSWORD`
environment variable.

As an example, with `ENVPASSWORD=password`, a secrets file could look like:

```yaml
foo:
  U2FsdGVkX1/j2VLVqN+2VdOgGi6KZcUWq3hmFZkxF20=
```

Pointing `CKI_SECRETS_FILE` to it and calling `cki_secret foo` will print the
decrypted value of the `foo` key:

```shell
$ cki_secret foo
bar
$ cki_secret foo --json
"bar"
```

For HashiCorp Vault, the following locations are understood:

- `no-slash-path`: return the value at `legacy/no-slash-path` for the `value` field
- `some/path`: return a dictionary of all key-value pairs at the path
- `some/path:field`: return only the value for the given field at the path

## Variables

For variables, the file name needs to be specified in the `CKI_VARS_FILE`
environment variable. A list of space-delimited additional environment variable
names can be provided in the `CKI_VARS_NAMES` environment variable. In contrast
to secrets, complex YAML values are supported.

As an example, a variables file could look like:

```yaml
foo: bar
baz: |
  some
  string
qux:
  complex: value
bool: true
int: 15
```

Pointing `CKI_VARS_FILE` to it and calling `cki_variable key` will print the
values of the various variables:

```shell
$ for i in foo baz qux bool int; do cki_variable $i; done
bar
some
string
{'complex': 'value'}
True
15
```

With `--json`, the output will be properly json-encoded:

```shell
$ for i in foo baz qux bool int; do cki_variable --json $i; done
"bar"
"some\nstring"
{"complex": "value"}
true
15
```
