# `shell-scripts/cki_deployment_acme.sh`

Update SSL certificates via dns-01 ACME challenges on Route 53, and deploy them
via OpenShift routes and SSH.

```shell
Usage: cki_deployment_acme.sh [certs|patch-remote|patch-local]
```

- `certs`: the certificates are checked and updated via the certificate authority if necessary
- `patch-remote`: existing certificates are deployed to remote clusters and via SSH
- `patch-local`: existing certificates are deployed to the local cluster

Openshift routes that should be managed need to be annotated with
`cki-project.org/acme: certificate-name`. This [certificate name] normally
corresponds to the first domain name in a line of the `ACME_DOMAINS` variable.

```yaml
apiVersion: route.openshift.io/v1
kind: Route
metadata:
  annotations:
    cki-project.org/acme: cki-project.org
```

| Environment variable          | Type   | Mode           | Required | Description                                                                                                      |
|-------------------------------|--------|----------------|----------|------------------------------------------------------------------------------------------------------------------|
| `CKI_DEPLOYMENT_ENVIRONMENT`  | string |                | no       | If set to "production", contact the production endpoint of Let's Encrypt certificate authority                   |
| `ACME_DOMAINS`                | string |                | yes      | Domain names for the certificates; certificates are issued per line for one or more space-separated domain names |
| `ACME_BUCKET`                 | string |                | yes      | Deployment-all style bucket specification for the backup of registration data, keys and certificates             |
| `ACME_PASSWORD`               | string |                | yes      | Password for the encryption of the backup tarballs                                                               |
| `ACME_AWS_ACCESS_KEY_ID`      | string | `certs`        | yes      | Access key of the AWS service account that is used to modify the Route 53 zones                                  |
| `ACME_AWS_SECRET_ACCESS_KEY`  | string | `certs`        | yes      | Secret key of the AWS service account that is used to modify the Route 53 zones                                  |
| `ACME_OPENSHIFT*_KEY`         | string | `patch-remote` | yes      | Secrets for the OpenShift service accounts to update the routes                                                  |
| `OPENSHIFT*_PROJECT`          | string | `patch-remote` | yes      | OpenShift API endpoints for the OpenShift service accounts                                                       |
| `OPENSHIFT*_SERVER`           | string | `patch-remote` | yes      | OpenShift projects for the OpenShift service accounts                                                            |
| `ACME_SSH_*_HOST`             | string | `patch-remote` | yes      | user@host for the ssh-able hosts where certs should be updated                                                   |
| `ACME_SSH_*_CERTIFICATE_NAME` | string | `patch-remote` | yes      | certificate name for the ssh-able hosts                                                                          |
| `ACME_SSH_*_PRIVATE_KEY_PATH` | string | `patch-remote` | yes      | full path to the secret key for the ssh-able hosts                                                               |
| `ACME_SSH_*_CERTIFICATE_PATH` | string | `patch-remote` | yes      | full path to the full certificate chain for the ssh-able hosts                                                   |
| `ACME_SSH_*_COMMAND`          | string | `patch-remote` | no       | command to run after certificate updates for the ssh-able hosts                                                  |
| `*_SSH_PRIVATE_KEY`           | string | `patch-remote` | yes      | Secret key for the ssh-able hosts                                                                                |

The AWS service account needs permissions equivalent to the following IAM policy:

```yaml
Version: '2012-10-17'
Statement:
  - Effect: Allow
    Action:
      - route53:ListHostedZonesByName
    Resource: '*'
  - Effect: Allow
    Action:
      - route53:ChangeResourceRecordSets
      - route53:ListResourceRecordSets
    Resource: arn:aws:route53:::hostedzone/ZONE-ID
```

The OpenShift service accounts need permissions equivalent to the following Role:

```yaml
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: acme-deployer
  labels:
    app: acme-deployer
rules:
  - apiGroups: [route.openshift.io]
    resources: [routes]
    verbs: [get, list, patch]
  - apiGroups: [route.openshift.io]
    resources: [routes/custom-host]
    verbs: [create]
```

For `patch-remote` mode, these permissions need to be specified via the
`OPENSHIFT_*` variables. In `patch-local` mode, these permissions must be
given to the service account of the pod.

[certificate alias]: https://github.com/dehydrated-io/dehydrated/blob/master/docs/domains_txt.md#wildcards
