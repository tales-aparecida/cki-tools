#!/bin/bash

set -Eeuo pipefail
shopt -s inherit_errexit

export START_PYTHON_SERVICE_METRICS="cki.cki_tools.service_metrics"
export LOG_NAME="service_metrics"

if [[ -r ${KRB_KEYTAB:-/keytab} ]]; then
    # shellcheck disable=SC2154
    kinit -t "${KRB_KEYTAB:-/keytab}" "${KRB_USER}" -l 7d
fi

exec cki_entrypoint.sh
