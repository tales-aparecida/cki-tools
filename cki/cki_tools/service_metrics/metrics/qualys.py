"""Qualys reports."""
import csv
import dataclasses
import datetime
import email
import functools
import io
import os
import poplib
import typing

import boto3
from cki_lib import s3bucket
from cki_lib.cronjob import CronJob
from cki_lib.logger import get_logger
import yaml

LOGGER = get_logger(__name__)
QUALYS_CONFIG = yaml.safe_load(os.environ.get('QUALYS_CONFIG', ''))


@dataclasses.dataclass
class QualysReport:
    """Represent a Qualys CSV report."""

    date: datetime.datetime
    raw: bytes

    @functools.cached_property
    def parsed(self) -> bytes:
        """Return a properly formated csv file."""
        reader = csv.reader(io.StringIO(self.raw.decode('utf8'), newline=''))
        writer = csv.writer(parsed_body := io.StringIO(newline=''))
        if not (header := next((row for row in reader if 'IP' in row), None)):
            raise Exception('Unable to find CSV header')
        writer.writerow(header)
        writer.writerows(reader)
        return parsed_body.getvalue().encode('utf8')


class QualysMetrics(CronJob):
    """Calculate Qualys report metrics."""

    schedule = '5 0 * * *'  # once a day

    @staticmethod
    def _upload_s3(report: QualysReport) -> None:
        date = report.date.astimezone(datetime.timezone.utc).date().isoformat()
        spec = s3bucket.parse_bucket_spec(os.environ[QUALYS_CONFIG['bucket']])
        LOGGER.debug('Uploading to %s/%s', spec.bucket, spec.prefix)
        client = boto3.Session().client('s3',
                                        aws_access_key_id=spec.access_key or None,
                                        aws_secret_access_key=spec.secret_key or None,
                                        endpoint_url=spec.endpoint or None)
        client.put_object(Bucket=spec.bucket, Key=f'{spec.prefix}raw/{date}.csv', Body=report.raw)
        client.put_object(Bucket=spec.bucket, Key=f'{spec.prefix}data.csv', Body=report.parsed)

    @staticmethod
    def _download_pop3() -> typing.Iterator[QualysReport]:
        pop3 = QUALYS_CONFIG['pop3']
        mailbox = poplib.POP3_SSL(pop3['host'], pop3['port'])
        mailbox.user(pop3['user'])
        mailbox.pass_(os.environ[pop3['pass']])
        for mail in mailbox.list()[1]:
            LOGGER.debug('Downloading %s', mail)
            mesg_num, octets = (int(i) for i in mail.split())
            msg = email.message_from_bytes(b'\n'.join(mailbox.top(mesg_num, octets)[1]))
            if csv_part := next((p for p in msg.walk() if
                                 p.get_content_disposition() == 'attachment' and
                                 p.get_content_type() == 'application/octet-stream' and
                                 'csv' in p.get_filename()), None):
                yield QualysReport(email.utils.parsedate_to_datetime(msg['Date']),
                                   csv_part.get_payload(decode=True))
            mailbox.dele(mesg_num)
        mailbox.quit()

    def update_qualys(self) -> None:
        """Update Qualys report data."""
        for report in self._download_pop3():
            self._upload_s3(report)

    def run(self, **_: typing.Any) -> None:
        """Download and and analyze Qualys metrics."""
        self.update_qualys()
