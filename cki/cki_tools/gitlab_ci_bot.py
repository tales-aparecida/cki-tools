"""Pipeline trigger for GitLab repositories."""
import argparse
import base64
import copy
import json
import logging
import os
import pathlib
import re
import sys
import traceback

from cki_lib import cki_pipeline
from cki_lib import misc
from cki_lib.logger import get_logger
from cki_lib.session import get_session
import dateutil.parser
import gitlab
import sentry_sdk
import yaml

LOGGER = get_logger('cki.cki_tools.gitlab_ci_bot')
SESSION = get_session('cki.cki_tools.gitlab_ci_bot')

# There are potentially three projects involved during the processing of a
# pipeline for a GitLab merge request:
# - pipeline_{gitlab,project}: pipeline repository
# - target_{gitlab,project}: pipeline-definition repository of CKI
# - target_gitlab, source_project: pipeline-definition fork for merge request

WELCOME_MESSAGE = """Hi! This is the friendly CKI test bot.

The maintainers can mention me in a top-level comment together with `test
[PIPELINES]` and I will test this merge request. Once the testing is done, I will
post the results here.

<details>
<summary>Click here for details on how to select the tests to run.</summary>

The testing is done by retriggering formerly successful pipelines with the new
code. Pipelines are specified by [GROUP], [GROUP/BRANCH] or [GROUP/PIPELINE-ID].

The following pipeline groups are configured:

{groups}

Within these groups, the following branches are configured:

{branches}

Triggering a group will trigger all configured branches for the group.

When triggering a pipeline for a branch, the last successful pipeline is
retriggered. The branches are not limited to the list above. Consult the
corresponding pipeline repository for the available branches.

The precise bot configuration can be found in
[cee/deployment-all](https://gitlab.cee.redhat.com/cki-project/deployment-all/-/blob/main/openshift/gitlab-ci-bot/10-configmap.yml.j2.d/config.yml).

</details>

<details>
<summary>Click here for details on how to adjust the functionality of the
retriggered pipeline by replacing the trigger variables of the original
pipeline.</summary>

Variables can be replaced by specifying `[name=value]` in the `test` command:

* Only run the pipeline for x86_64 and s390x: `[architectures=x86_64 s390x]`
* Use the latest container images: `[image_tag=latest]`
* Run the tests in Beaker (please use this functionality responsibly and
  conserve the resources if the test runs are not needed):
  `[skip_beaker=false]`
* Force a full pipeline via `[tests_only=false]` or a short pipeline with only
  the setup and testing stages via `[tests_only=true]`.
* Code coverage analysis:
  * For RHEL, please use `[gcov/<branch>][skip_beaker=false]`
  * For CentOS, please use `[centos-gcov/<branch>][skip_beaker=false]`,
    with the appropriate branch (`c9s`, `c9s-auto`)
  * To limit coverage to certain kernel directories add `[coverage_dirs=dirs1 dirs2]`.
* Limited test set: `[test_set=net]`
* Filter test names (bot uses raw comment so don't escape special characters):
  `[tests_regex=.*Networking ipsec.*]`
* Force an MR run with targeted testing: `[force_baseline=false]`
* If needed, a closing square bracket can be specified by \\u005D

The full list of available variables can be found in the
[CKI documentation](https://cki-project.org/docs/user_docs/configuration/).

</details>

For more details about the features, check out the
[bot documentation](https://cki-project.org/docs/operations/pipeline-bot/).

{welcome_message}
"""


def first_awardemoji(bot_login, comment):
    """Return the first bot-authored emoji, or None."""
    return next((emoji for emoji in comment.awardemojis.list(iterator=True)
                 if emoji.user['username'] == bot_login), None)


def all_done(pipelines):
    """Check whether all pipelines are done.

    This includes the GitLab states (all but pending/running) and the internal
    states from start_pipelines.
    """
    done = ('success', 'failed', 'canceled', 'skipped',
            'not-found', 'internal-error', 'merge-conflict')
    return all(p['status'] in done for p in pipelines)


def get_status_note(bot_login, discussion):
    """Return the first bot-authored note in the discussion, or None."""
    note_id = next((n['id'] for n in discussion.attributes['notes'][1:]
                    if n['author']['username'] == bot_login), None)
    return discussion.notes.get(note_id) if note_id else None


def format_status_table(pipelines, welcome_note_url):
    """Return a markdown table with the pipeline status."""
    raw = base64.encodebytes(json.dumps(pipelines).encode('utf8'))
    rows = (['| Group | Branch | ID | Status |\n| --- | --- | --- | --- |\n'] +
            [format_status_table_row(pipeline) for pipeline in pipelines] +
            [f'\n<small>See the [welcome message]({welcome_note_url}) for details '
             'on how to run tests more selectively.</small>\n'] +
            [f'<!-- {raw.decode("ascii")} -->'])
    return ''.join(rows)


def format_status_table_row(pipeline):
    """Return a markdown table row with the pipeline status."""
    pipeline_label = pipeline.get('pipeline_label')
    pipeline_branch = pipeline.get('pipeline_branch')
    pipeline_id = pipeline.get('id')
    if 'web_url' in pipeline:
        pipeline_id = f'[{pipeline_id}]({pipeline["web_url"]})'
    status_icon = {
        'created': ':hourglass_flowing_sand:',
        'pending': ':hourglass_flowing_sand:',
        'running': ':hourglass_flowing_sand:',
        'canceled': ':grey_exclamation:',
        'success': ':heavy_check_mark:',
        'not-found': ':grey_question:',
    }.get(pipeline['status'], ':exclamation:')
    pipeline_status = f'{status_icon} {pipeline["status"]}'
    if pipeline.get('stage'):
        pipeline_status += f' ([{pipeline["stage"]}]({pipeline["job"]}))'
    return (f'| {pipeline_label} | {pipeline_branch} ' +
            f'| {pipeline_id} | {pipeline_status} |\n')


def format_status_list(pipelines):
    """Return a plain list with the pipeline status."""
    rows = [format_status_list_item(pipeline) for pipeline in pipelines]
    return ''.join(rows)


def format_status_list_item(pipeline):
    """Return a list item row with the pipeline status."""
    pipeline_label = pipeline.get('pipeline_label')
    pipeline_branch = pipeline.get('pipeline_branch')
    pipeline_id = pipeline.get('web_url') or pipeline.get('id')
    pipeline_status = pipeline.get('status')
    return (f'- {pipeline_id} ({pipeline_label}/{pipeline_branch}): ' +
            f'{pipeline_status}\n')


def set_status_note(discussion, note, pipelines, welcome_note_url, force_notify=False):
    """Post the status of the pipelines in comment form.

    The comment contains a visible table and the raw data base64-encoded in a
    hidden HTML comment.

    If no note is passed, a new note is created at the end of the discussion.
    Notes that cause emails are first created with plain text, and then changed
    to the table.
    """
    body = format_status_table(pipelines, welcome_note_url)
    if not note:
        # an email notification will be sent, so create it plain-text first
        note = discussion.notes.create(
            {'body': format_status_list(pipelines)})
        note.body = body
        note.save()
    else:
        note.body = body
        note.save()
        if force_notify:
            # force a plain-text email notification
            note = discussion.notes.create(
                {'body': format_status_list(pipelines)})
            note.delete()


def parse_status_note(body):
    """Parse the raw data of the status note."""
    match = re.search(r'(?<=<!-- ).*(?= -->)', body, flags=re.DOTALL)
    if not match:
        return {}
    return json.loads(base64.decodebytes(match.group(0).encode('utf8')))


def format_short_names(short_names):
    """Return a list of pipeline groups."""
    return ''.join(f'[{n}]' for n in short_names)


def format_long_name(pipeline_config):
    """Return a plural-form pipeline group name with optional description."""
    name = f'{pipeline_config["name"]}s'
    if 'description' in pipeline_config:
        name += f' ({pipeline_config["description"]})'
    return name


def format_groups(pipelines_config, default_pipelines):
    """Return a markdown list of pipeline groups."""
    items = [f'* `[{p}]`: {format_long_name(pipelines_config[p])}\n'
             for p in pipelines_config.keys()]
    return ''.join(items + [f'* `[all]`: same as `{format_short_names(default_pipelines)}`\n'])


def format_branch_list(pipelines_config):
    """Format a markdown list of pipeline branches."""
    items = [f'* `[{p}/{branch}]`\n'
             for p in pipelines_config.keys()
             for branch in pipelines_config[p]['default_branches']]
    return ''.join(items)


def create_welcome_note(pipelines_config, project_config, merge_request) -> int:
    """Create a note with the welcome comment."""
    values = {
        'groups': format_groups(pipelines_config, project_config['default_pipelines']),
        'branches': format_branch_list(pipelines_config),
        'welcome_message': project_config.get('welcome_message', ''),
    }
    return merge_request.notes.create({'body': WELCOME_MESSAGE.format(**values)}).id


def update_pipelines(pipelines_config, pipelines):
    """Update the status of the pipelines from GitLab."""
    pipelines = copy.deepcopy(pipelines)
    for pipeline in pipelines:
        with misc.only_log_exceptions():
            pipeline_label = pipeline['pipeline_label']
            project_name = pipeline['project']
            pipeline_id = pipeline['id']
            if not pipeline_id:
                continue
            pipeline_config = pipelines_config[pipeline_label]
            token = os.environ[pipeline_config['private_token']]
            with gitlab.Gitlab(pipeline_config['gitlab_url'],
                               private_token=token,
                               session=SESSION) as pipeline_gitlab:
                pipeline_project = pipeline_gitlab.projects.get(project_name)
                project_pipeline = pipeline_project.pipelines.get(pipeline_id)
                pipeline['status'] = project_pipeline.status
                if project_pipeline.status in ('failed', 'running'):
                    if gl_jobs := project_pipeline.jobs.list(
                            scope=project_pipeline.status, per_page=1, all=False):
                        pipeline['job'] = gl_jobs[0].web_url
                        pipeline['stage'] = gl_jobs[0].stage
    return pipelines


def parse_commands(body):
    """Extract and preprocess user commands from the comment note body.

    Supports:
    - [all] -> results['all'] = True
    - [group] -> results['groups'] = ['group', ...]
    - [group/abc] -> results['branches'] = [('group', 'abc'), ...]
    - [key=value] -> results['variables'] = {'key': 'value'}
    """
    raw_commands = re.findall(r'(?<=\[)([^]]+?)(?=\\?\])', body)
    commands = {
        'all': False,
        'groups': [],
        'branches': [],
        'variables': {},
    }
    for raw_command in raw_commands:
        stripped = raw_command.strip()
        if stripped == 'all':
            commands['all'] = True
        elif '=' in stripped:
            parts = stripped.split('=', 1)
            commands['variables'][parts[0].strip()] = parts[1].strip().replace(r'\u005D', ']')
        elif '/' in stripped:
            parts = stripped.split('/', 1)
            commands['branches'].append((parts[0].strip(), parts[1].strip()))
        else:
            commands['groups'].append(stripped)
    return commands


def _add_branches(results, pipelines_config, group_filter=None):
    for pipeline_label, pipeline_config in pipelines_config.items():
        if group_filter and pipeline_label not in group_filter:
            continue
        for branch in pipeline_config['default_branches']:
            results.add((pipeline_label, branch))


def tested_branches(pipelines_config, project_config, commands):
    """Determine the projects and branches to test.

    Commands are processed in the following way:

    - if [all] is given, all default branches of all groups configured in
      default_pipelines will be added to the test set.
    - if [group] is given, all default branches for the specified group
      configured in the pipelines_config will be added to the test set.
    - if [group/abc] is given, the branch/pipeline ID for the specified group
      will be added to the test set.
    - if none of the above commands is given, all default branches of all
      groups configured in default_pipelines will be added to the test set.
    """
    results = set()
    if commands['all']:
        _add_branches(results, pipelines_config,
                      group_filter=project_config['default_pipelines'])
    if commands['groups']:
        _add_branches(results, pipelines_config,
                      group_filter=commands['groups'])
    if commands['branches']:
        results |= set(commands['branches'])
    return results


def start_pipeline(pipeline_config, pipeline_branch, variable_overrides):
    """Trigger and return one pipeline, or return an error string."""
    try:
        token = os.environ[pipeline_config['private_token']]
        variable_filter = pipeline_config.get('variable_filter', {})
        virtual_branches = pipeline_config.get('virtual_branches', {})
        with gitlab.Gitlab(pipeline_config['gitlab_url'],
                           private_token=token,
                           session=SESSION) as pipeline_gitlab:
            pipeline_project = pipeline_gitlab.projects.get(
                pipeline_config['project'])
            try:
                original_pipeline_id = int(pipeline_branch)
                pipeline_project.pipelines.get(original_pipeline_id)
            except gitlab.exceptions.GitlabGetError:
                return 'not-found'
            except ValueError:
                if pipeline_branch in virtual_branches:
                    virtual_branch = virtual_branches[pipeline_branch]
                    pipeline_branch = virtual_branch['branch']
                    variable_filter.update(virtual_branch.get('variable_filter', {}))
                original_pipeline = cki_pipeline.last_successful_pipeline_for_branch(
                    pipeline_project, pipeline_branch,
                    variable_filter=variable_filter)
                if not original_pipeline:
                    return 'not-found'
                original_pipeline_id = original_pipeline.id
            return cki_pipeline.retrigger(
                pipeline_project,
                original_pipeline_id,
                variable_overrides=variable_overrides)
    except Exception:  # pylint: disable=broad-except
        traceback.print_exc()
        return 'internal-error'


def build_variable_overrides(pipeline_config, project_config, target_gitlab, merge_request):
    # pylint: disable=too-many-locals
    """Build the variable overrides for the retriggered pipeline."""
    variables = {}

    source_project = target_gitlab.projects.get(merge_request.source_project_id)
    target_project = target_gitlab.projects.get(merge_request.target_project_id)
    override_config = project_config.get('override', {})
    override_type = override_config.get('type')
    override_ref = override_config.get('ref', 'merge')

    if override_ref == 'branch':
        repo_id = source_project.id
        repo_url = source_project.web_url
        ref_name = merge_request.source_branch
        ref_sha = merge_request.sha
    elif override_ref in ('head', 'merge'):
        repo_id = target_project.id
        repo_url = target_project.web_url
        ref_name = f'refs/merge-requests/{merge_request.iid}/{override_ref}'
        try:
            ref_commit = target_project.commits.get(ref_name)
        except Exception:
            LOGGER.exception('MR-under-test has no valid commit %s', ref_name)
            raise Exception('merge-conflict') from None
        ref_sha = ref_commit.id
        if (override_ref == 'merge' and
            target_project.commits.get(f'refs/merge-requests/{merge_request.iid}/head').id
                not in ref_commit.parent_ids):
            LOGGER.warning('MR-under-test has outdated head commit')
            raise Exception('merge-conflict')
    else:
        LOGGER.error('Unknown override ref type %s', override_ref)
        raise Exception('internal-error')

    if override_type == 'pipeline-definition':
        variables.update({
            'pipeline_definition_repository_override': repo_url,
            'pipeline_definition_branch_override': ref_sha,
        })
    elif override_type == 'archive_url':
        name = override_config['name']
        variables[name] = (f'{target_gitlab.api_url}/projects/' +
                           f'{repo_id}/repository/archive.zip?sha={ref_sha}')
    elif override_type == 'pip_url':
        name = override_config['name']
        variables[name] = f'git+{repo_url}.git/@{ref_name}'
    elif override_type == 'image_tag':
        names = override_config.get('names') or [override_config['name']]
        for name in names:
            variables[name] = f'mr-{merge_request.iid}'
    elif override_type is not None:
        LOGGER.error('Unknown override type %s', override_type)
        raise Exception('internal-error')

    variables.update(pipeline_config.get('variables', {}))
    variables.update(project_config.get('variables', {}))
    variables = {key: str(value) for key, value in variables.items()}

    return variables


def start_pipelines(pipelines_config, project_config, target_gitlab,
                    merge_request, note):
    """Trigger the appropriate pipelines."""
    commands = parse_commands(note.body)
    test_set = tested_branches(pipelines_config, project_config, commands)
    results = []
    for pipeline_label, pipeline_branch in sorted(test_set):
        try:
            pipeline_config = pipelines_config[pipeline_label]
        except KeyError:
            continue
        print(f'processing {pipeline_branch} of {pipeline_label}')
        result = {'pipeline_branch': pipeline_branch,
                  'id': '',
                  'status': 'internal-error',
                  'pipeline_label': pipeline_label,
                  'project': pipeline_config['project']}
        results.append(result)

        try:
            variable_overrides = build_variable_overrides(
                pipeline_config, project_config, target_gitlab, merge_request)
        # pylint: disable=broad-except
        except Exception as exc:
            result['status'] = exc.args[0]
            continue

        variable_overrides.update(commands['variables'])
        pipeline = start_pipeline(pipeline_config, pipeline_branch, variable_overrides)
        if isinstance(pipeline, str):
            result['status'] = pipeline
        else:
            result['id'] = pipeline.id
            result['status'] = pipeline.status
            result['web_url'] = pipeline.web_url
    return results


def is_developer(project, user_id):
    """Return whether a user is at least developer of a project."""
    matching_members = project.members_all.list(per_page=1, user_ids=[user_id])
    return matching_members and matching_members[0].access_level >= 30


def was_force_pushed(target_gitlab, merge_request, since):
    """Check if the MR was updated by a force push after the specified time."""
    source_project_id = merge_request.source_project_id
    source_project = target_gitlab.projects.get(source_project_id)
    since_datetime = dateutil.parser.parse(since)

    for event in source_project.events.list(iterator=True):
        if dateutil.parser.parse(event.created_at) < since_datetime:
            return False

        # This catches adding more commits too but we want to complain about
        # that as well so it should be ok
        if event.action_name == 'pushed_to' and \
                event.push_data['commit_from'] is not None and \
                event.push_data['ref'] == merge_request.source_branch:
            return True

    return False


def was_committed(merge_request, since):
    """Check if the MR has any new commits since given datetime."""
    since_datetime = dateutil.parser.parse(since)
    for commit in merge_request.commits():
        if dateutil.parser.parse(commit.created_at) > since_datetime:
            return True
    return False


def note_awardemoji(note, name, previous_awardemoji=None):
    """Add an emoji to a note.

    The emojis are used to keep state for a note:
    - thumbsup: pipelines have been triggered, and the pipeline status is
      updated in a follow-up note
    - checkered_flag: final results from all pipelines are available
    - thumbsdown: the testing request in the note is invalid
    """
    if previous_awardemoji:
        previous_awardemoji.delete()
    note.awardemojis.create({'name': name})


def process_merge_request(pipelines_config, project_config, target_gitlab,
                          target_project, merge_request):
    """Process the bot interactions in a merge request."""
    inttest_id = os.getenv('IT_GITLAB_MERGE_REQUEST')
    if inttest_id and inttest_id != str(merge_request.iid):
        return
    logging.info('Checking MR #%s from %s',
                 merge_request.attributes['iid'],
                 project_config['project'])
    discussions = merge_request.discussions.list(get_all=True)
    bot_login = target_gitlab.user.username
    welcome_note_id = next((
        misc.get_nested_key(d.attributes, 'notes/0/id')
        for d in discussions
        if not misc.get_nested_key(d.attributes, 'notes/0/system') and
        misc.get_nested_key(d.attributes, 'notes/0/author/username') == bot_login
    ), None)
    if not welcome_note_id:
        welcome_note_id = create_welcome_note(pipelines_config, project_config, merge_request)
    welcome_note_url = f'{merge_request.web_url}#note_{welcome_note_id}'
    for discussion in discussions:
        with misc.only_log_exceptions():
            if not discussion.attributes['notes'][0]['system']:
                process_discussion(
                    pipelines_config, project_config, target_gitlab,
                    target_project, merge_request, discussion, welcome_note_url)


# pylint: disable=too-many-arguments
def process_discussion(pipelines_config, project_config, target_gitlab,
                       target_project, merge_request, discussion, welcome_note_url):
    """Process a single bot interaction in a merge request."""
    bot_login = target_gitlab.user.username
    first_comment = discussion.attributes['notes'][0]
    body = first_comment['body']
    if f'@{bot_login}' in body and 'test' in body:
        note = merge_request.notes.get(first_comment['id'])
        awardemoji = first_awardemoji(bot_login, note)
        if awardemoji:
            if awardemoji.name == 'thumbsup':
                status_note = get_status_note(bot_login, discussion)
                if status_note:
                    pipelines = parse_status_note(status_note.body)
                    updated = update_pipelines(pipelines_config, pipelines)
                    if all_done(updated):
                        note_awardemoji(note, 'checkered_flag', awardemoji)
                        set_status_note(discussion, status_note,
                                        updated, welcome_note_url, force_notify=True)
                    else:
                        set_status_note(discussion, status_note, updated, welcome_note_url)
                else:
                    # cannot check pipelines without a status note, so stop
                    note_awardemoji(note, 'checkered_flag', awardemoji)
            return

        if not is_developer(target_project, note.author['id']):
            note_awardemoji(note, 'thumbsdown')
            discussion.notes.create({
                'body':
                f'Hi {note.author["username"]}! You don\'t have '
                'permissions to trigger testing. Please wait for a '
                'maintainer to review your PR.'
            })
            return

        if (was_force_pushed(target_gitlab, merge_request, note.created_at)
                or was_committed(merge_request, note.created_at)):
            note_awardemoji(note, 'thumbsdown')
            discussion.notes.create({
                'body':
                'Hi! The PR code has been modified since testing was '
                'requested. Please review the new changes before asking '
                'again to test.'
            })
            return

        # If we got here it means pipelines should be triggered
        pipelines = start_pipelines(pipelines_config, project_config,
                                    target_gitlab, merge_request, note)

        if not pipelines:
            note_awardemoji(note, 'thumbsdown')
            discussion.notes.create({
                'body':
                'Hi! I could not find pipelines matching your request. '
                'Please try again :hugging:'
            })
            return

        status_note = get_status_note(bot_login, discussion)
        set_status_note(discussion, status_note,
                        pipelines, welcome_note_url, force_notify=True)
        note_awardemoji(note, 'thumbsup')


def main(args):
    """Process the bot interactions in all open merge requests."""
    misc.sentry_init(sentry_sdk)

    parser = argparse.ArgumentParser(description='Pipeline trigger for GitLab repositories')
    parser.add_argument('--config-path',
                        default=os.environ.get('GITLAB_CI_BOT_CONFIG_PATH', 'config.yml'),
                        help='Path to the config file')
    parsed_args = parser.parse_args(args)

    module_config = yaml.safe_load(os.environ.get('GITLAB_CI_BOT_CONFIG') or
                                   pathlib.Path(parsed_args.config_path).read_text(encoding='utf8'))

    pipelines_config = module_config['pipelines']
    for project_config in module_config['projects'].values():
        token = os.environ[project_config['private_token']]
        with gitlab.Gitlab(project_config['gitlab_url'],
                           private_token=token,
                           session=SESSION) as target_gitlab:
            target_gitlab.auth()
            target_project_path = project_config['project']
            target_project = target_gitlab.projects.get(target_project_path)
            for merge_request in target_project.mergerequests.list(
                    state='opened', iterator=True):
                with misc.only_log_exceptions():
                    process_merge_request(pipelines_config, project_config,
                                          target_gitlab, target_project,
                                          merge_request)


if __name__ == '__main__':
    main(sys.argv[1:])
