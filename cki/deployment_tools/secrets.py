"""Implement secrets and variable access as used in the infrastructure repo."""
import argparse
import functools
import json
import os
import pathlib
import subprocess
import sys
import typing

from cki_lib.session import get_session
import hvac
import yaml

SESSION = get_session('cki.deployment_tools.secrets')


@functools.lru_cache()
def _read_secrets_file(file_path: str) -> typing.Dict[str, typing.Any]:
    secrets = yaml.safe_load(pathlib.Path(file_path).read_text(encoding='utf8'))
    if not isinstance(secrets, dict):
        raise Exception('Invalid secrets file')
    return secrets


def _read_secrets_files(env_names: typing.Sequence[str]) -> typing.Dict[str, typing.Any]:
    result = {}
    for env_name in env_names:
        if env_name not in os.environ:
            continue
        result.update(_read_secrets_file(os.environ[env_name]))
    return result


def encrypt(value: str, *, salt: bool = True) -> str:
    """Encrypt a secret."""
    process = subprocess.run([
        'openssl', 'enc',
        '-aes-256-cbc',
        '-md', 'sha512',
        '-pbkdf2',
        '-iter', '100000',
        '-salt' if salt else '-nosalt',
        '-pass', 'env:ENVPASSWORD',
        '-e', '-a'
    ], encoding='utf8', input=value, stdout=subprocess.PIPE, check=True)
    return process.stdout.strip()


def decrypt(value: str, *, salt: bool = True) -> str:
    """Decrypt a secret."""
    process = subprocess.run([
        'openssl', 'enc',
        '-aes-256-cbc',
        '-md', 'sha512',
        '-pbkdf2',
        '-iter', '100000',
        '-salt' if salt else '-nosalt',
        '-pass', 'env:ENVPASSWORD',
        '-d', '-a'
    ], encoding='utf8', input=value + '\n', stdout=subprocess.PIPE, check=True)
    return process.stdout.strip()


def secret(key: str) -> typing.Any:
    """Return a decrypted secret from the secrets file."""
    if encrypted_value := _read_secrets_files(
            os.environ.get('CKI_SECRETS_NAMES', '').split() + ['CKI_SECRETS_FILE']).get(key):
        return decrypt(encrypted_value)
    if not os.environ.get('VAULT_ADDR'):
        raise Exception(f'Secret {key} not found in secrets files')

    client = hvac.Client(session=SESSION)
    path, _, field = key.partition(':')
    if '/' not in path:
        path, field = f'legacy/{path}', 'value'
    data = client.secrets.kv.read_secret(f'cki/{path}', 'apps')['data']['data']
    return data[field] if field else data


def variable(key: str) -> typing.Any:
    """Return an unencrypted variable from the variables files."""
    value = _read_secrets_files(
        os.environ.get('CKI_VARS_NAMES', '').split() + ['CKI_VARS_FILE'])[key]
    if isinstance(value, str) and value.startswith('U2FsdGVkX1'):
        raise Exception(f'Unable to access secret {key} via get_variable')
    return value.strip() if isinstance(value, str) else value


def _encrypt_cli(argv: typing.Optional[typing.List[str]] = None) -> None:
    """Encrypt a secret via the CLI."""
    _main(['encrypt'] + (argv or sys.argv[1:]))


def _secret_cli(argv: typing.Optional[typing.List[str]] = None) -> None:
    """Return a decrypted secret from the secrets file via the CLI."""
    _main(['secret'] + (argv or sys.argv[1:]))


def _variable_cli(argv: typing.Optional[typing.List[str]] = None) -> None:
    """Return an unencrypted variable from the variables files via the CLI."""
    _main(['variable'] + (argv or sys.argv[1:]))


def _main(argv: typing.Optional[typing.List[str]] = None) -> None:
    """Access the CKI secrets tools."""
    parser = argparse.ArgumentParser(description='Access CKI variables and secrets')
    parser.add_argument('--json', action='store_true', help='output in json format')
    parser.add_argument('type', choices=('encrypt', 'secret', 'variable'))
    parser.add_argument('value')
    args = parser.parse_args(argv)

    result = globals()[args.type](args.value)
    print(json.dumps(result) if args.json else result)


if __name__ == '__main__':
    _main()
