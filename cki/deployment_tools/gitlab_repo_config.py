"""Check GitLab repos for the recommended configuration."""

import argparse
import datetime
import functools
import os
import pathlib
import re
import sys
from urllib import parse

from cki_lib import config_tree
from cki_lib import gitlab
from cki_lib import logger
from cki_lib import misc
import dateutil.parser
from gitlab import exceptions as gl_exceptions
import sentry_sdk
import yaml

LOGGER = logger.get_logger('cki.deployment_tools.gitlab_repo_config')


class RepoConfig:
    """Check GitLab repos for the recommended configuration."""

    def __init__(self, fix=False):
        """Create a new checker."""
        self._fix = fix

    @staticmethod
    @functools.lru_cache
    # lru_cache is ok here as static methods don't suffer from the issues described at
    # https://rednafi.github.io/reflections/dont-wrap-instance-methods-with-functoolslru_cache-decorator-in-python.html
    def _instance(instance_url):
        return gitlab.get_instance(instance_url)

    @staticmethod
    @functools.lru_cache
    # lru_cache is ok here as static methods don't suffer from the issues described at
    # https://rednafi.github.io/reflections/dont-wrap-instance-methods-with-functoolslru_cache-decorator-in-python.html
    def _group(instance_url, group):
        return RepoConfig._instance(instance_url).groups.get(group)

    @staticmethod
    @functools.lru_cache
    # lru_cache is ok here as static methods don't suffer from the issues described at
    # https://rednafi.github.io/reflections/dont-wrap-instance-methods-with-functoolslru_cache-decorator-in-python.html
    def _project(project_url):
        url_parts = parse.urlsplit(project_url)
        instance_url = parse.urlunparse(url_parts[:2] + ('',) * 4)
        project_path = url_parts.path[1:]
        return RepoConfig._instance(instance_url).projects.get(project_path)

    def discover_group_projects(self, url, group):
        """Return a set of all projects in the given group."""
        return {p.web_url for p in self._group(url, group).projects.list(iterator=True)
                # the path check shouldn't be necessary, but otherwise
                # https://gitlab.com/CentOS/automotive/src/kernel/kernel-automotive-9 is found 😱
                if not p.archived and p.path_with_namespace.startswith(group)}

    def discover_config_projects(self, configs, only_project_url=None):
        """Return a dict of project name -> config for all projects mentioned in the config."""
        projects = {}
        for name, config in configs.items():
            if name.endswith('/default'):
                if only_project_url:
                    if only_project_url.startswith(f'{config["url"]}/{config["group"]}'):
                        projects.setdefault(only_project_url, config)
                else:
                    for url in self.discover_group_projects(config['url'], config['group']):
                        projects.setdefault(url, config)
            else:
                project_url = f'{config["url"]}/{config["group"]}/{name.split("/")[-1]}'
                if not only_project_url or only_project_url == project_url:
                    projects[project_url] = config
        LOGGER.debug('Discovered: %s', projects.keys())
        return projects

    def check_fork_ci(self, gl_project):
        """Check that forks have CI disabled."""
        for fork in gl_project.forks.list(iterator=True, owned=True):
            gl_fork = gl_project.manager.gitlab.projects.get(fork.id)
            if gl_fork.jobs_enabled:
                print(f'  unexpected enabled CI in fork {gl_fork.path_with_namespace}')
                if self._fix:
                    print('  fixing')
                    gl_fork.jobs_enabled = False
                    gl_fork.save()

    def check_project_creation(self, gl_group, config):
        """Check that nobody can create new projects."""
        if not config.get('project_creation_level'):
            return
        if gl_group.project_creation_level != config['project_creation_level']:
            print(f'  unexpected project creation level {gl_group.project_creation_level}')
            if self._fix:
                print('  fixing')
                gl_group.project_creation_level = config['project_creation_level']
                gl_group.save()

    @staticmethod
    def check_tags(gl_project, config):
        """Check that there are no tags."""
        tags = set(b.name for b in gl_project.tags.list(iterator=True))
        for tag_pattern in (config.get('tags') or []):
            tags -= {b for b in tags if re.fullmatch(tag_pattern, b)}
        if config['tags'] is not None and tags:
            print(f'  unexpected tags {tags}')

    @staticmethod
    def check_branches(gl_project, config):
        """Check that there are no branches but the default branch."""
        branches = set(b.name for b in gl_project.branches.list(iterator=True))
        branches.discard(gl_project.default_branch)
        for branch_pattern in (config.get('branches') or []):
            branches -= {b for b in branches if re.fullmatch(branch_pattern, b)}
        if config['branches'] is not None and branches:
            print(f'  unexpected branches {branches}')

    def check_project_field(self, gl_project, field_name, expected):
        """Check that private builds are configured correctly."""
        LOGGER.debug('Checking %s for %s', gl_project.path, field_name)
        if field_name not in gl_project.attributes:
            LOGGER.warning('Field %s not found', field_name)
            return
        if gl_project.attributes[field_name] != expected:
            print(f'  unexpected {field_name}: {gl_project.attributes[field_name]} != {expected}')
            if self._fix:
                print('  fixing')
                setattr(gl_project, field_name, expected)
                gl_project.save()

    @staticmethod
    def _access_level_dicts(data):
        """Convert GitLab API access level dicts to something comparable."""
        return {key: {
            k: {
                'user_ids': {a['user_id'] for a in v if a.get('user_id') is not None},
                'group_ids': {a['group_id'] for a in v if a.get('group_id') is not None},
                'access_level': next((a['access_level'] for a in v
                                      if a.get('user_id') is None and a.get('group_id') is None),
                                     None),
            } if k.endswith('_levels') else v
            for k, v in values.items()
        } for key, values in data.items()}

    @staticmethod
    def _access_level_filter(data, expected):
        """Only keep the "interesting" fields in access level dicts."""
        return {key: {k: v for k, v in values.items()
                      if key not in expected or k in expected[key]}
                for key, values in data.items()}

    @classmethod
    def _access_level_equal(cls, data, expected):
        """Compare tag/branch access levels."""
        dict_data = cls._access_level_dicts(data)
        dict_expected = cls._access_level_dicts(expected)
        return cls._access_level_filter(dict_data, dict_expected) == dict_expected

    @classmethod
    def _access_level_create(cls, data):
        """Prepare the POST data to create tag/branch protection rules."""
        dict_datas = cls._access_level_dicts(data)
        results = []
        for name, config in data.items():
            result = {'name':  name}
            for key, value in config.items():
                if key.endswith('_levels'):
                    dict_data = dict_datas[name][key]
                    if not dict_data['user_ids'] and not dict_data['group_ids']:
                        result[key[:-1]] = dict_data['access_level']
                    else:
                        result['allowed_to_' + key.split('_', 1)[0]] = value
                else:
                    result[key] = value
            results.append(result)
        return results

    def check_protectedtags(self, gl_project, config):
        """Check that tag protection is configured correctly."""
        protectedtags = {t.name: t.attributes
                         for t in gl_project.protectedtags.list(iterator=True)}
        expected = config.get('protectedtags', {})
        if not self._access_level_equal(protectedtags, expected):
            print(f'  unexpected tag protections {protectedtags} != {expected}')
            if self._fix:
                print('  fixing')
                for protectedtag in gl_project.protectedtags.list(iterator=True):
                    protectedtag.delete()
                for data in self._access_level_create(expected):
                    gl_project.protectedtags.create(data)

    def check_protectedbranches(self, gl_project, config):
        """Check that branch protection is configured correctly."""
        protectedbranches = {b.name: b.attributes
                             for b in gl_project.protectedbranches.list(iterator=True)}
        expected = config.get('protectedbranches', {})
        if not self._access_level_equal(protectedbranches, expected):
            print(f'  unexpected branch protections {protectedbranches} != {expected}')
            if self._fix:
                print('  fixing')
                for protectedbranch in gl_project.protectedbranches.list(iterator=True):
                    protectedbranch.delete()
                for data in self._access_level_create(expected):
                    gl_project.protectedbranches.create(data)

    def check_pushrules(self, gl_project, config):
        """Check that pushrules are configured correctly."""
        try:
            gl_pushrules = gl_project.pushrules.get()
        except gl_exceptions.GitlabGetError:
            return

        expected = config.get('pushrules', {})
        if {k: v for k, v in gl_pushrules.attributes.items() if k in expected} != expected:
            print('  unexpected pushrules settings')
            if self._fix:
                print('  fixing')
                gl_project.pushrules.update(**expected)

    def check_approvals(self, gl_project, config):
        """Check that approvals are configured correctly."""
        try:
            gl_approvals = gl_project.approvals.get()
        except gl_exceptions.GitlabGetError:
            return

        expected = config.get('approvals', {})

        if expected.get('selective_code_owner_removals'):
            try:
                gl_project.files.get('CODEOWNERS', ref='main')
            except gl_exceptions.GitlabGetError:
                print('  falling back to resetting all approvals without CODEOWNERS')
                expected['reset_approvals_on_push'] = True
                expected['selective_code_owner_removals'] = False

        if {k: v for k, v in gl_approvals.attributes.items() if k in expected} != expected:
            print(f'  unexpected approval settings: {gl_approvals.attributes} != {expected}')
            if self._fix:
                print('  fixing')
                gl_project.approvals.update(**expected)

        expected_rules = config.get('approvalrules', {})
        gl_approvalrules = {r.name: r for r in gl_project.approvalrules.list(iterator=True)}

        for name, expected in expected_rules.items():
            if not (gl_approvalrule := gl_approvalrules.get(name)):
                print(f'  missing approval rule {name}')
                if self._fix:
                    print('  fixing')
                    gl_project.approvalrules.create({'name': name, **expected})
                continue

            gl_approvalrule.user_ids = [g['id'] for g in gl_approvalrule.users]
            gl_approvalrule.group_ids = [g['id'] for g in gl_approvalrule.groups]
            if {k: v for k, v in gl_approvalrule.attributes.items() if k in expected} != expected:
                print(f'  unexpected approval settings in rule {name}')
                if self._fix:
                    print('  fixing')
                    gl_project.approvalrules.update(gl_approvalrule.id, expected)

        for name in set(gl_approvalrules.keys()) - set(expected_rules.keys()):
            print(f'  superfluous approval rule {name}')
            if self._fix:
                print('  fixing')
                gl_approvalrules[name].delete()

    def check_environments(self, gl_project, config):
        """Check that no stale environments exist."""
        if gl_project.operations_access_level == 'disabled':
            return
        expired = []

        cutoff = (datetime.datetime.utcnow().astimezone(datetime.timezone.utc) -
                  datetime.timedelta(days=config['environments']['expire_days']))
        try:
            result = gitlab.get_graphql_client(gl_project.manager.gitlab.url).query(
                '''
                query ($fullPath: ID!, $after: String = "") {
                    project(fullPath: $fullPath) {
                        environments(after: $after) {
                            nodes {
                                id name state createdAt
                                lastDeployment(status: SUCCESS) { updatedAt }
                            }
                            pageInfo { hasNextPage endCursor }
                        }
                    }
                }
                ''',
                variable_values={'fullPath': gl_project.path_with_namespace},
                paged_key='project/environments')
        except Exception:  # pylint: disable=broad-except
            # GitLab version might not support the requested GraphQL Environment nodes
            print('  unable to list environments, outdated GitLab version?')
            return

        for environment in misc.get_nested_key(result, 'project/environments/nodes'):
            if any(re.fullmatch(e, environment['name']) for e in config['environments']['keep']):
                continue
            if dateutil.parser.parse(environment['createdAt']) > cutoff:
                continue
            if ((updated_at := misc.get_nested_key(environment, 'lastDeployment/updatedAt'))
                    and dateutil.parser.parse(updated_at) > cutoff):
                continue
            print(f'  expired environment {environment["name"]}')
            expired.append(environment)
        if self._fix and expired:
            print('  fixing')
            for environment in expired:
                gl_environment = gl_project.environments.get(environment['id'].split('/')[-1],
                                                             lazy=True)
                if environment['state'] != 'stopped':
                    gl_environment.stop()
                try:
                    gl_environment.delete()
                except gl_exceptions.GitlabDeleteError:
                    print(f'  unable to delete {environment["name"]}')

    def _delete_container_images_once(self, gl_repository, rules) -> bool:
        deleted = False
        result = gitlab.get_graphql_client(gl_repository.manager.gitlab.url).query(
            '''
            query($id: ContainerRepositoryID!, $after: String="") {
                containerRepository(id: $id) {
                    tags(after: $after) {
                        nodes { name createdAt }
                        pageInfo { hasNextPage endCursor }
                    }
                }
            }
            ''',
            variable_values={'id': f'gid://gitlab/ContainerRepository/{gl_repository.id}'},
            paged_key='containerRepository/tags')
        tags = {t['name']: t for t in misc.get_nested_key(result, 'containerRepository/tags/nodes')}
        keep_tags = set()
        for tag_pattern, rule in rules.items():
            matching_tags = sorted((t for t in tags.values()
                                    if t['createdAt'] and re.fullmatch(tag_pattern, t['name'])),
                                   key=lambda t: t['createdAt'], reverse=True)
            keep_n = (rule or {}).get('keep_n')
            keep_younger = (rule or {}).get('keep_younger')
            if keep_n is not None or keep_younger is not None:
                if keep_n is not None:
                    keep_tags |= {t['name'] for t in matching_tags[:keep_n]}
                if keep_younger is not None:
                    cutoff = (datetime.datetime.utcnow().astimezone(datetime.timezone.utc) -
                              misc.parse_timedelta(keep_younger))
                    keep_tags |= {t['name'] for t in matching_tags
                                  if dateutil.parser.parse(t['createdAt']) > cutoff}
            else:
                keep_tags |= {t['name'] for t in matching_tags}
        for tag in keep_tags:
            tags.pop(tag, None)
        for tag in tags:
            print(f'  outdated {tag}')
        if self._fix and tags:
            print('  fixing')
            for tag in tags:
                print(f'  removing {tag}')
                deleted = True
                try:
                    gl_repository.tags.delete(tag)
                except gl_exceptions.GitlabDeleteError:
                    print(f'  unable to delete {tag}')
        return deleted

    def check_container_images(self, gl_project, config):
        """Check that no stale container images exist."""
        if not gl_project.container_registry_enabled or not config.get('container_registry'):
            return

        for gl_repository in gl_project.repositories.list(iterator=True):
            print(f'  processing container images at {gl_repository.name}')
            registries = [rules for registry_pattern, rules in config['container_registry'].items()
                          if re.fullmatch(registry_pattern, gl_repository.name)]
            if len(registries) > 1:
                print(f'  unable to process {gl_repository.name} matching multiple rules')
                continue
            rules = registries[0] if registries else config['container_registry']['default']
            if not rules:
                continue
            # loop until there is nothing deleted anymore
            # at the moment, tags.list() will only return the first 100 tags 🙈
            while self._delete_container_images_once(gl_repository, rules):
                pass

    def run(self, configs, only_project_url=None):
        """Run all checkers."""
        print('Discovering projects')
        projects = self.discover_config_projects(configs, only_project_url)
        for project_url, config in projects.items():
            gl_project = self._project(project_url)
            if config['ignore']:
                LOGGER.debug('Ignoring %s', project_url)
                continue

            print(f'Processing {project_url}')
            gl_group = self._group(gl_project.manager.gitlab.url, gl_project.namespace['full_path'])
            self.check_project_creation(gl_group, config)
            self.check_protectedtags(gl_project, config)
            self.check_protectedbranches(gl_project, config)
            self.check_tags(gl_project, config)
            self.check_branches(gl_project, config)
            self.check_fork_ci(gl_project)
            self.check_approvals(gl_project, config)
            self.check_pushrules(gl_project, config)
            self.check_environments(gl_project, config)
            self.check_container_images(gl_project, config)
            for field, expected in config['project'].items():
                self.check_project_field(gl_project, field, expected)


def main(args):
    """Run the main CLI interface."""
    misc.sentry_init(sentry_sdk)
    parser = argparse.ArgumentParser()
    parser.add_argument('--fix', action='store_true',
                        help='Fix detected problems')
    parser.add_argument('--project-url',
                        help='Only run the checker for one project')
    parser.add_argument('--config-path',
                        default=os.environ.get('GITLAB_REPO_CONFIG_PATH', 'config.yml'),
                        help='Path to the config file')
    parsed_args = parser.parse_args(args)

    configs = config_tree.process_config_tree(yaml.safe_load(
        os.environ.get('GITLAB_REPO_CONFIG') or
        pathlib.Path(parsed_args.config_path).read_text(encoding='utf8')))

    RepoConfig(fix=parsed_args.fix).run(configs, only_project_url=parsed_args.project_url)


if __name__ == '__main__':
    main(sys.argv[1:])
