"""Try to match a message to a deployment."""
import re

from cki_lib import config_tree
from cki_lib import misc
from cki_lib.gitlab import get_instance

from . import utils


class Matcher:
    # pylint: disable=too-few-public-methods
    """Try to match a message to a deployment."""

    def __init__(self, project_config, project_url, body):
        """Create a new matcher."""
        self.project_config = project_config
        self.project_url = project_url
        self.body = body

        self.instance_url, self.path_with_namespace = utils.parse_project_url(project_url)

        self.captures = {'project_url': self.project_url,
                         'instance_url': self.instance_url,
                         'path': self.path_with_namespace.split('/')[-1],
                         'path_with_namespace': self.path_with_namespace}
        self.captures.update(self.project_config['.trigger'].get('captures', {}))

    def match(self):
        """Find a suitable deployment config.

        Returns the formatted variables.
        """
        match = re.fullmatch(self.project_config['.trigger']['project_url'], self.project_url)
        if not match:
            return None
        self.captures.update(match.groupdict())

        deployment_config = next((
            v for v in config_tree.clean_config(self.project_config).values()
            if self._matches(self.body, v['trigger'])), None)
        if not deployment_config:
            return None

        variables = dict(deployment_config['deployment']['variables'])

        self.captures['job_url'] = misc.shorten_url(
            '{project_url}/-/jobs/{job_id}'.format_map(self.captures))

        external_url = (deployment_config['deployment'].get('external_url') or
                        self.project_config['.deployment'].get('external_url'))
        if external_url:
            variables['deployment_bot_external_url'] = external_url

        variables['deployment_bot_message_success'] = misc.get_nested_key(
            deployment_config, 'deployment/message/success', '')
        variables['deployment_bot_message_failed'] = misc.get_nested_key(
            deployment_config, 'deployment/message/failed', '')

        with get_instance(self.instance_url) as gl_instance:
            job_id = int(self.captures['job_id'])
            gl_project = gl_instance.projects.get(self.path_with_namespace, lazy=True)
            gl_environment = self.environment(gl_project, job_id)
            if gl_environment:
                self.captures['environment_id'] = gl_environment.id
                self.captures['environment'] = gl_environment.name
                variables['deployment_bot_environment_url'] = (
                    '{project_url}/-/environments/{environment_id}')
            else:
                self.captures['environment'] = 'unknown'

            gl_job = gl_project.jobs.get(job_id)
            self.captures['commit_message'] = self.commit_message(gl_project, gl_job)

        return {k: v.format_map(self.captures) for k, v in variables.items()}

    def _matches(self, first, second):
        """Compare for equality with the partial dict of second."""
        if first is None:
            return False
        if isinstance(second, dict):
            return all(self._matches(first.get(key), second[key])
                       for key in second.keys())
        match = re.fullmatch(second, str(first))
        if not match:
            return False
        self.captures.update(match.groupdict())
        return True

    @staticmethod
    def environment(gl_project, job_id):
        """Return the environment for a job."""
        for gl_environment in gl_project.environments.list(states='available', iterator=True):
            gl_full_environment = gl_project.environments.get(gl_environment.id)
            last_deployment = gl_full_environment.last_deployment
            if (last_deployment and last_deployment.get('deployable') and
                    last_deployment['deployable']['id'] == job_id):
                return gl_full_environment
        return None

    @staticmethod
    def commit_message(gl_project, gl_job):
        """Return a clean MR/commit message for a job."""
        for match in re.finditer('^refs/merge-requests/(.*)/merge$', gl_job.ref):
            return gl_project.mergerequests.get(int(match.group(1))).title

        mrs = gl_project.commits.get(gl_job.commit['id'], lazy=True).merge_requests()
        return mrs[0]['title'] if mrs else gl_job.commit['title']
