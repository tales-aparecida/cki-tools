"""Listen for UMB messages and submit them to Datawarehouse."""
import os

from cki_lib import messagequeue
from cki_lib import metrics
from cki_lib import misc
import datawarehouse
from kcidb_io.schema import V4
import prometheus_client as prometheus
import sentry_sdk

METRIC_UMB_RESULTS = prometheus.Counter(
    'umb_results', 'Number of UMB results submitted to datawarehouse')


class Receiver:
    # pylint: disable=too-few-public-methods
    """Provides callback to submit to Datawarehouse."""

    def __init__(self):
        """Initialize Datawarehouse object used for every callback."""
        self.dw_api = datawarehouse.Datawarehouse(os.environ['DATAWAREHOUSE_URL'],
                                                  os.environ['DATAWAREHOUSE_TOKEN_SUBMITTER'])

    def callback(self, body, **_):
        """Submit a UMB result message to datawarehouse."""
        try:
            V4.validate(body)
        # don't want to add jsonschema dependency to catch jsonschema.exceptions.ValidationError
        # pylint: disable=broad-except
        except Exception as exception:
            sentry_sdk.capture_exception(exception)
            return

        self.dw_api.kcidb.submit.create(data=body)
        METRIC_UMB_RESULTS.inc()


def main():
    """Submit UMB results to datawarehouse."""
    misc.sentry_init(sentry_sdk)
    metrics.prometheus_init()
    receiver = Receiver()

    connection = messagequeue.MessageQueue()
    connection.consume_messages(
        os.environ['RABBITMQ_EXCHANGE'],
        os.environ['RABBITMQ_ROUTING_KEYS'].split(),
        receiver.callback,
        queue_name=os.environ['RABBITMQ_QUEUE']
    )


if __name__ == '__main__':
    main()
