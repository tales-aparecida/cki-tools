"""Regexes to look for bugs 🐛🐛."""
from contextlib import contextmanager
from functools import lru_cache
import re
import time

from cki_lib.logger import get_logger
import prometheus_client as prometheus

from cki.triager import dw_client
from cki.triager import settings
from cki.triager import utils

LOGGER = get_logger('cki.triager.regexes')
METRIC_REGEX_SEARCH_TIME = prometheus.Histogram(
    'regex_search_time_seconds',
    'Time spent looking through a log file with the regexes'
)
METRIC_REGEX_MATCH_TIME = prometheus.Summary(
    'regex_match_time_seconds',
    'Time spent matching, grouped by regex.',
    ['regex_id']
)


class RegexChecker:
    # pylint: disable=too-few-public-methods
    """RegexChecker."""

    def __init__(self):
        """Init."""
        self.lookups = []

    @staticmethod
    def _compile_lookups(lookups):
        """Compile the regexes."""
        LOGGER.debug('Compiling lookups')
        compiled_lookups = []
        for lookup in lookups:
            for field_name in ('text_match',
                               'test_name_match',
                               'file_name_match',
                               'tree_match',
                               'kpet_tree_name_match',
                               'architecture_match'):
                field = getattr(lookup, field_name)
                compiled_field = re.compile(field, re.DOTALL) if field else None
                setattr(lookup, f'compiled_{field_name}', compiled_field)

            compiled_lookups.append(lookup)

        return compiled_lookups

    def download_lookups(self, issueregex_ids=None):
        """Call self._download_lookups with cache hash."""
        self.lookups = self._download_lookups(
            ttl_hash=utils.get_cache_ttl(),
            # list is not hashable, tuple is necessary for lru_cache
            issueregex_ids=tuple(issueregex_ids or []),
        )

    @staticmethod
    @lru_cache(maxsize=1)
    # lru_cache is ok here as static/class methods don't suffer from the issues described at
    # https://rednafi.github.io/reflections/dont-wrap-instance-methods-with-functoolslru_cache-decorator-in-python.html
    def _download_lookups(ttl_hash, issueregex_ids=None):
        """Download and compile regexes from Datawarehouse."""
        del ttl_hash  # Not used, just a param to invalidate lru_cache
        LOGGER.debug('Downloading lookups. issueregex_ids: %s', issueregex_ids)
        if issueregex_ids:
            regexes = [
                dw_client.issue_regex.get(id=issueregex_id)
                for issueregex_id in issueregex_ids
            ]
        else:
            regexes = dw_client.issue_regex.list(limit=200)

        lookups = RegexChecker._compile_lookups(regexes)
        LOGGER.debug('Found %i lookups', len(lookups))
        return lookups

    @staticmethod
    def _match(lookup, get_text, log, obj):
        # pylint: disable=too-many-return-statements
        """Match lookup against log."""
        text_match = lookup.compiled_text_match
        test_name_match = lookup.compiled_test_name_match
        file_name_match = lookup.compiled_file_name_match
        architecture_match = lookup.compiled_architecture_match
        tree_match = lookup.compiled_tree_match
        kpet_tree_name_match = lookup.compiled_kpet_tree_name_match

        if test_name_match and not (obj.comment and test_name_match.search(obj.comment)):
            LOGGER.debug('Test name "%s" did not match: %s', test_name_match, obj.comment)
            return False

        if file_name_match and not (log.get('name') and file_name_match.search(log['name'])):
            LOGGER.debug('File name "%s" did not match: %s', file_name_match, log.get('name'))
            return False

        text = get_text()
        with RegexChecker._match_time_measure(lookup):
            if text_match and not (text and text_match.search(text)):
                return False

        # We got this far, do the expensive queries.
        # This should probably go somewhere else, and we need a better way to
        # identify the obj type. DWObject has a obj_type, but dw-api-lib doesnt.
        # We can check isinstance() but that doesnt work for DWObject.
        if obj.checkout_id:  # obj is a build
            build = obj
        elif obj.build_id:
            build = utils.get_build(dw_client, obj.build_id, utils.get_cache_ttl())
        else:
            # It's a checkout, it doesn't have builds.
            build = None

        if architecture_match and not (
                build and
                build.architecture and
                architecture_match.search(build.architecture)):
            return False

        if obj.tree_name:  # obj is a checkout
            checkout = obj
        elif build:
            checkout = utils.get_checkout(dw_client, build.checkout_id, utils.get_cache_ttl())
        else:
            checkout = None

        if tree_match and not (
                checkout and
                checkout.tree_name and
                tree_match.search(checkout.tree_name)):
            return False

        if kpet_tree_name_match and not (
                build and
                build.misc.get('kpet_tree_name') and
                kpet_tree_name_match.search(build.misc['kpet_tree_name'])):
            return False

        # Fallback. Found it.
        return True

    @staticmethod
    @contextmanager
    def _match_time_measure(lookup):
        """Measure the time matching a regex."""
        start = time.time()
        try:
            yield
        finally:
            elapsed = time.time() - start
            METRIC_REGEX_MATCH_TIME.labels(regex_id=lookup.id).observe(elapsed)
            if elapsed > 1:
                # Arbitrary value, tuning might be necessary
                LOGGER.error("Regex matching took too long. regex_id=%d elapsed_s=%f",
                             lookup.id, elapsed)

    @METRIC_REGEX_SEARCH_TIME.time()
    def search(self, get_text, log, obj):
        """Search for regexes ocurrences on text."""
        issues = []
        LOGGER.debug('Searching in %s', log['url'])
        for lookup in self.lookups:
            if not self._match(lookup, get_text, log, obj):
                continue

            result = {'name': lookup.issue['description'], 'id': lookup.issue['id'],
                      'regex_id': lookup.id}
            LOGGER.debug('Found %s', result)
            issues.append(result)

        return issues or settings.NOT_FOUND
