"""Triager module."""
from cki_lib.logger import get_logger
from restclient import APIManager

from cki.triager import dw_client
from cki.triager import settings
from cki.triager.checkers import BuildFailureChecker
from cki.triager.checkers import CheckoutFailureChecker
from cki.triager.checkers import FailureChecker
from cki.triager.checkers import TestFailureChecker

LOGGER = get_logger('cki.triager.triager')


class DWObject:
    # pylint: disable=too-few-public-methods
    """Mock Datawarehouse api lib from a dict."""

    def __init__(self, obj_type, attrs):
        """Init."""
        self.type = obj_type
        self.attrs = attrs
        self._api = APIManager(settings.DATAWAREHOUSE_URL, settings.DATAWAREHOUSE_TOKEN)

    def __getattr__(self, name):
        """Override getattr to treat dict elements as object attributes."""
        try:
            return self.attrs[name]
        except KeyError:
            return self.__dict__.get(name)

    def issues_create(self, issue_id):
        """Create issue for the object."""
        obj_iid = self.attrs['misc']['iid']

        self._api.post(f'/api/1/kcidb/{self.type}s/{obj_iid}/issues', {'issue_id': issue_id})

    def issues_list(self):
        """Return list of issues."""
        obj_iid = self.attrs['misc']['iid']

        return self._api.get(f'/api/1/kcidb/{self.type}s/{obj_iid}/issues')['results']

    def action_triaged(self):
        """Tag this object as triaged."""
        obj_iid = self.attrs['misc']['iid']

        self._api.post(f'/api/1/kcidb/{self.type}s/{obj_iid}/actions/triaged', {})


class Triager:
    """Triage a kcidb object."""

    def __init__(self, dry_run=False):
        """Init."""
        self.dry_run = dry_run

    @staticmethod
    def check_checkout(checkout, misc=None):
        """Check checkout for known issues."""
        LOGGER.debug('Checking checkout id=%s', checkout.id)

        if checkout.valid:
            return []

        return CheckoutFailureChecker.check(checkout, misc)

    @staticmethod
    def check_build(build, misc=None):
        """Check build for known issues."""
        LOGGER.debug('Checking build id=%s', build.id)

        if build.valid:
            return []

        return BuildFailureChecker.check(build, misc)

    @staticmethod
    def check_test(test, misc=None):
        """Check test for known issues."""
        LOGGER.debug('Checking test id=%s', test.id)

        if test.status not in ['ERROR', 'FAIL']:
            return []

        return TestFailureChecker.check(test, misc)

    @staticmethod
    def check_testresult(testresult, misc=None):
        """Check testresult for known issues."""
        LOGGER.debug('Checking testresult id=%s', testresult.id)

        if testresult.status not in ['ERROR', 'FAIL']:
            return []

        return FailureChecker.check(testresult, misc)

    @staticmethod
    def get_issue_ids(obj):
        """Return the list of issue ids linked to the object."""
        if isinstance(obj, DWObject):
            return [i['id'] for i in obj.issues_list()]

        return [i.id for i in obj.issues.list()]

    def report_issues(self, obj, issues):
        """Report a list of issues."""
        already_tagged_issues = self.get_issue_ids(obj)
        for issue in issues:
            if issue['id'] in already_tagged_issues:
                LOGGER.debug('Issue id=%s is already linked to id=%s', issue, obj.id)
                continue

            LOGGER.info('Linking issue id=%s to id=%s', issue, obj.id)

            if self.dry_run:
                LOGGER.info('Dry run. Skipping issue reporting.')
                continue

            method = obj.issues_create if isinstance(obj, DWObject) else obj.issues.create
            method(issue_id=issue['id'])

    def check(self, obj_type, obj, misc=None):
        """Check object for issues."""
        methods = {
            'checkout': self.check_checkout,
            'build': self.check_build,
            'test': self.check_test,
            'testresult': self.check_testresult,
        }

        if isinstance(obj, str):
            # Treat obj as the id and query for the object.
            obj = getattr(dw_client.kcidb, f'{obj_type}s').get(obj)
        elif isinstance(obj, dict):
            # Mock datawarehouse lib to avoid extra API calls.
            obj = DWObject(obj_type, obj)

        issues = methods[obj_type](obj, misc)
        if issues:
            self.report_issues(obj, issues)

        if obj_type in ('test', 'testresult'):
            finished = obj.status is not None
        else:
            finished = obj.valid is not None

        if finished and not self.dry_run:
            # Tag this object as triaged
            (obj.action_triaged if isinstance(obj, DWObject) else obj.action_triaged.create)()

        return issues
